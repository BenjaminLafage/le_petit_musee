package com.example.lepetitmusee;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class JSONResponseHandlerObject {
    private static final String TAG = JSONResponseHandlerObject.class.getSimpleName();
    private List<Object> itemList;

    public JSONResponseHandlerObject(List<Object> itemList){
        this.itemList = itemList;
    }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readArrayObject(reader);
        } finally {
            reader.close();
        }
    }

    private void readArrayObject(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            Object object = new Object(reader.nextName());
            reader.beginObject();
            while(reader.hasNext()){
                String name = reader.nextName();

                if (name.equals("name")) {
                    object.setName(reader.nextString());
                } else if (name.equals("categories")) {
                    reader.beginArray();
                    List<String> categories = new ArrayList<String>();
                    while (reader.hasNext()) {
                        categories.add(reader.nextString());
                    }
                    reader.endArray();
                    object.setCategories(categories);
                } else if (name.equals("description")) {
                    object.setDescription(reader.nextString());
                } else if (name.equals("timeFrame")) {
                    reader.beginArray();
                    List<Integer> timeFame = new ArrayList<Integer>();
                    while (reader.hasNext()) {
                        timeFame.add(reader.nextInt());
                    }
                    reader.endArray();
                    object.setTimeFrame(timeFame);
                } else if (name.equals("year")) {
                    object.setYear(reader.nextInt());
                } else if (name.equals("brand")) {
                    object.setBrand(reader.nextString());
                } else if (name.equals("working")) {
                    if(reader.nextBoolean()){
                        object.setWorking(1);
                    }
                }
                else {
                    reader.skipValue();
                }
            }
            object.setThumbnail(WebServiceUrl.buildThumbnail(object.getId()).toString());
            itemList.add(object);
            reader.endObject();
        }
    }
}
