package com.example.lepetitmusee;

import android.content.Context;
import android.media.Image;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toolbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.squareup.picasso.Picasso;


public class ObjectActivity extends AppCompatActivity {
    TextView name, brand, description, year, working, category;
    ImageView thumbnail;
    private Object object;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object);
        object = getIntent().getParcelableExtra(Object.TAG);

        name = (TextView) findViewById(R.id.NameDetail);
        brand = (TextView) findViewById(R.id.brandView);
        description = (TextView) findViewById(R.id.descriptionView);
        year = (TextView) findViewById(R.id.yearView);
        working = (TextView) findViewById(R.id.workingView);
        category = (TextView) findViewById(R.id.CategoriesDetail);
        thumbnail = (ImageView) findViewById(R.id.ThumbnailDetail);
        updateView();
    }

    void updateView(){
        name.setText(object.getName());
        category.setText(object.category());
        brand.setText(object.getBrand());
        description.setText(object.getDescription());
        working.setText(object.working());
        if (object.getThumbnail() != null && !object.getThumbnail().isEmpty()) {
            Picasso.with(this).load(object.getThumbnail()).into(thumbnail);
        } else {
            thumbnail.setImageResource(R.mipmap.ic_launcher);
        }
    }

}
