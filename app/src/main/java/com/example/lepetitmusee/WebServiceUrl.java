package com.example.lepetitmusee;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrl {
    private static final String HOST = "demo-lia.univ-avignon.fr";
    private static final String PATH_1 = "cerimuseum";

    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1);
        return builder;
    }

    private static final String OBJECTS = "ids";
    public static URL buildIds() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(OBJECTS);
        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String OBJECT_CATEGORIES = "categories";
    public static URL buildCategories() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(OBJECT_CATEGORIES);
        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String CATALOG = "catalog";
    public static URL buildCatalog() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(CATALOG);
        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String OBJECT = "items";
    public static URL buildObject(String idObject) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(OBJECT)
                .appendPath(idObject);
        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String THUMBNAIL = "thumbnail";
    public static URL buildThumbnail(String idObject) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(OBJECT)
                .appendPath(idObject)
                .appendPath(THUMBNAIL);
        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String IMAGES = "images";
    public static URL buildImages(String idObject, String idImage) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(OBJECT)
                .appendPath(idObject)
                .appendPath(IMAGES)
                .appendPath(idImage);
        URL url = new URL(builder.build().toString());
        return url;
    }

    private static final String DEMOS = "demos";
    public static URL buildDemos() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(DEMOS);
        URL url = new URL(builder.build().toString());
        return url;
    }
}
