package com.example.lepetitmusee;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ObjectAdapter extends RecyclerView.Adapter<ObjectAdapter.ObjectViewHolder> implements Filterable {
    List<Object> itemList;
    List<Object> itemListTmp;
    private Context context;
    private ClickListener clickListener;


    public ObjectAdapter(List<Object> itemList, Context context) {
        this.itemList = itemList;
        this.itemListTmp = itemList;
        this.context = context;
    }

    public void setTeamListAdapter(List<Object> itemList) {
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public ObjectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_row,parent,false);
        return new ObjectViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ObjectViewHolder holder, int position) {
        try {
            holder.display(itemListTmp.get(position));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return itemListTmp.size();
    }

    public Object getObjectAtPosition(int position) {return itemListTmp.get(position);}

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String ch = constraint.toString();
                if(ch.isEmpty()){
                    itemListTmp = itemList;
                }
                else {
                    List<Object> filter = new ArrayList<>();
                    for(Object object : itemList){
                        if(object.getName().toLowerCase().contains(ch.toLowerCase())) {
                            filter.add(object);
                        }
                        else {
                            for(int i = 0; i < object.getCategories().size(); i++){
                                if(object.getCategories().get(i).contains(ch.toLowerCase())){
                                    filter.add(object);
                                }
                            }
                        }
                    }
                    itemListTmp = filter;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = itemListTmp;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                itemListTmp = (List<Object>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ClickListener{
        void onItemClick(int position,View v);
    }
    public void setOnItemClickListener(ClickListener clickListener){
        this.clickListener = clickListener;
    }


    public class ObjectViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView objectNameHolder;
        private TextView objectBrandHolder;
        private ImageView objectImageHolder;
        private TextView objectCategoryHolder;

        public ObjectViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            objectNameHolder = itemView.findViewById(R.id.objectName);
            objectBrandHolder = itemView.findViewById(R.id.objectBrand);
            objectImageHolder = itemView.findViewById(R.id.objectImage);
            objectCategoryHolder = itemView.findViewById(R.id.objectCategory);
        }

        void display(Object objectHolder) throws IOException {
            objectNameHolder.setText(objectHolder.getName());
            objectBrandHolder.setText(objectHolder.getBrand());
            objectCategoryHolder.setText(objectHolder.category());
            if(objectHolder.getThumbnail() != null && !objectHolder.getThumbnail().isEmpty()) {
                Picasso.with(itemView.getContext()).load(objectHolder.getThumbnail()).into(objectImageHolder);
            }
            else {
                objectImageHolder.setImageResource(R.mipmap.ic_launcher);
            }

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }
}
