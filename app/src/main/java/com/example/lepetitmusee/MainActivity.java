package com.example.lepetitmusee;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getSimpleName();
    public static List<Object> itemList= new ArrayList<Object>();

    private RecyclerView recyclerView;
    private ObjectAdapter adapter;
    public SwipeRefreshLayout swipeRefreshLayout;
    private SearchView itemView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);

        recyclerView = findViewById(R.id.itemRecyclerView);
        if(itemList.size() == 0){
            Worker worker = new Worker(itemList,MainActivity.this);
            try {
                worker.execute(WebServiceUrl.buildCatalog());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }


        adapter = new ObjectAdapter(itemList, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener((position, v) -> {
            Intent intent = new Intent(MainActivity.this, ObjectActivity.class);
            intent.putExtra(Object.TAG, adapter.getObjectAtPosition(position));
            MainActivity.this.startActivity(intent);

        });

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            if(itemList.size() == 0) {
                Worker worker = new Worker(itemList,MainActivity.this);
                try {
                    worker.execute(WebServiceUrl.buildCatalog());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
            else {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }
    public void update(){
        recyclerView.setAdapter(adapter);
    }

    public boolean OnCreateOptionMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tri, menu);
        MenuItem item = menu.findItem(R.id.app_bar_search);
        itemView = (SearchView) item.getActionView();
        itemView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }

    public boolean onOptionItemSelected(MenuItem menuItem) {
        Object tmp;
        int id = menuItem.getItemId();
        if(id==R.id.app_bar_search){
            return true;
        }
        if(id == R.id.tri_alphabet){
            for(int i = 0; i < itemList.size(); i++){
                if(itemList.get(i).getName().compareTo(itemList.get(i + 1).getName()) > 0){
                    tmp = itemList.get(i);
                    itemList.set(i,itemList.get(i+1));
                    itemList.set(i+1,tmp);
                    i -= 1;
                }
            }
            update();
            return true;
        }
        if(id == R.id.tri_chrono){
            for(int i = 0; i < itemList.size(); i++){
                if(itemList.get(i).getYear() > itemList.get(i+1).getYear()){
                    tmp = itemList.get(i);
                    itemList.set(i,itemList.get(i+1));
                    itemList.set(i+1,tmp);
                    i -= 1;
                }
            }
            update();
            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
