package com.example.lepetitmusee;

import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Worker extends AsyncTask<URL, Integer, ArrayList<Object>> {
    static List<Object> itemList;
    private Context context;

    public Worker(List<Object> itemList, Context context){
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    protected ArrayList<Object> doInBackground(URL... urls) {
        JSONResponseHandlerObject handlerObject = new JSONResponseHandlerObject(this.itemList);
        HttpURLConnection connectionItems;
        InputStream inputStreamItems;
        try {
            connectionItems = (HttpURLConnection) urls[0].openConnection();
            inputStreamItems = new BufferedInputStream(connectionItems.getInputStream());
            try{
                handlerObject.readJsonStream(inputStreamItems);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return (ArrayList<Object>) this.itemList;
    }

    @Override
    protected void onPostExecute(ArrayList<Object> itemList) {
        super.onPostExecute(itemList);
        if(context instanceof MainActivity){
            ((MainActivity) context).update();
            ((MainActivity) context).swipeRefreshLayout.setRefreshing(false);
        }
    }
}
