package com.example.lepetitmusee;

import android.content.Intent;
import android.graphics.Picture;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

import java.util.HashMap;
import java.util.List;

public class Object implements Parcelable {
    private String id;
    private List<String> categories;
    private String name;
    private String description;
    private int year;
    private String brand;
    private List<Integer> timeFrame;
    private List<String> technicalDetails;
    private Integer working;
    HashMap<String, String> pictures;
    private String thumbnail;

    public static final String TAG = Object.class.getSimpleName();

    public Object(String id) {
        this.id = id;
        this.year = 0;
        this.working = 0;
    }

    public Object(String id, List<String> categories, String name, String description, List<Integer> timeFrame) {
        this.id = id;
        this.categories = categories;
        this.name = name;
        this.description = description;
        this.timeFrame = timeFrame;
        this.year = 0;
    }


    public static final Creator<Object> CREATOR = new Creator<Object>() {
        @Override
        public Object createFromParcel(Parcel in) {
            return new Object(in);
        }

        @Override
        public Object[] newArray(int size) {
            return new Object[size];
        }
    };

    public String getId() {
        return id;
    }

    public List<String> getCategories() {
        return categories;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getYear() {
        return year;
    }

    public String getBrand() {
        return brand;
    }

    public List<Integer> getTimeFrame() {
        return timeFrame;
    }

    public List<String> getTechnicalDetails() {
        return technicalDetails;
    }

    public Integer getWorking() {
        return working;
    }

    public HashMap<String, String> getPictures() {
        return pictures;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setTimeFrame(List<Integer> timeFrame) {
        this.timeFrame = timeFrame;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public void setTechnicalDetails(List<String> technicalDetails) {
        this.technicalDetails = technicalDetails;
    }

    public void setWorking(Integer working) {
        this.working = working;
    }



    public String category() {
        String category = "";
        for(int i = 0; i < this.categories.size(); i++){
            category += this.categories.get(i);
        }
        return category;
    }

    public String working() {
        if(working != 1) {
            return "Non fonctionnel";
        }
        else {
            return "Fonctionnel";
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeList(categories);
        dest.writeString(description);
        dest.writeList(timeFrame);
        dest.writeInt(year);
        dest.writeString(brand);
        if(working!=null){
            dest.writeInt(working);
        }
        dest.writeString(thumbnail);
    }

    private Object(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        in.readList(this.categories,String.class.getClassLoader());
        this.description = in.readString();
        in.readList(this.timeFrame,Integer.class.getClassLoader());
        this.year = in.readInt();
        this.brand = in.readString();
        this.working = in.readInt();
        this.thumbnail = in.readString();
    }

}
